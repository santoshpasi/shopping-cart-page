window.onload = function(){
	//cart box
	const iconShopping = document.querySelector('.iconShopping');
	const cartCloseBtn = document.querySelector('.fa-close');
	const cartBox = document.querySelector('.cartBox');
	iconShopping.addEventListener("click",function(){
		cartBox.classList.add('active');
		// window.open("checkout.html", "_self");
	});
	cartCloseBtn.addEventListener("click",function(){
		cartBox.classList.remove('active');
	});
	// debugger;

	cartUpdate();

	// adding data to localstorage
	const attToCartBtn = document.getElementsByClassName('attToCart');
	// debugger;
	let items = [];
	// let price = 0;
	for(let i=0; i<attToCartBtn.length; i++){
		attToCartBtn[i].addEventListener("click",function(e){
			if(typeof(Storage) !== 'undefined'){
				let item = {
						id:i+1,
						name:e.target.parentElement.children[0].textContent,
						price:e.target.parentElement.children[1].textContent,
						no:1
					};
				if(JSON.parse(localStorage.getItem('items')) === null){
					items.push(item);
					localStorage.setItem("items",JSON.stringify(items));
					window.location.reload();
				}else{
					const localItems = JSON.parse(localStorage.getItem("items"));
					localItems.map(data=>{
						if(item.id == data.id){
							item.no = data.no + 1;
						}else{
							items.push(data);
						}
					});
					items.push(item);
					localStorage.setItem('items',JSON.stringify(items));
					window.location.reload();
				}
			}else{
				alert('local storage is not working on your browser');
			}
		});
	}

	// adding data to shopping cart 
	const iconShoppingP = document.querySelector('.iconShopping p');
	let no = 0;
	JSON.parse(localStorage.getItem('items')).map(data=>{
		no = no+data.no;
	});
	iconShoppingP.innerHTML = no;

	$(document).ready(function(){
		$(".itemsRow").on('click','.btnDelete',function(e){
			let items = [];
			var id = $(this).attr("data-id");
			// console.log("id",id);
			JSON.parse(localStorage.getItem('items')).map(data=>{
				if(data.id != id){
					items.push(data);
				}
			});
			localStorage.setItem('items',JSON.stringify(items));
			cartUpdate();
			window.location.reload();
		 });
	});

	function cartUpdate(){
		const cardBoxTable = cartBox.querySelector('table');
		let tableData = '';
			tableData += '<tr>'+
							'<th>S no.</th>'+
							'<th>Item Name</th>'+
							'<th>Item No</th>'+
							'<th>item Price</th>'+
							'<th>Remove</th>'+
						'</tr>';
		if(JSON.parse(localStorage.getItem('items'))[0] === null){
			tableData += '<tr><td colspan="5">No items found</td></tr>'
		}else{
		JSON.parse(localStorage.getItem('items')).map(data=>{
			tableData += '<tr class="itemsRow">'+
							'<th>'+data.id+'</th>'+
							'<th>'+data.name+'</th>'+
							'<th>'+data.no+'</th>'+
							'<th>'+data.price+'</th>'+
							'<th><a class="btnDelete" data-id="'+data.id+'">Delete</a></th>'+
						'</tr>'
			});
		}
		cardBoxTable.innerHTML = tableData;
	}

	//  onclick=Delete(this);
	// class="btnDelete"

	//adding cartBox data in table

}

function NewTab() {
	window.open("cart.html", "_self");
}